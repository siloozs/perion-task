import "./App.css";
import { useCallback, useState, useRef } from "react";
import SearchForm from "./components/SearchForm/SearchFrom";
import List from "./components/List/List";
import axios from "axios";

export default function App() {
  const inputRef = useRef(null);
  const [list, setList] = useState([]);

  const search = useCallback((e) => {
    const term = inputRef.current.value;
    axios
      .get(`https://www.reddit.com/search.json?q=${term}&sort=new`)
      .then((result) => {
        setList(result.data.data.children);
      });
  }, []);

  return (
    <div className="App">
      <SearchForm ref={inputRef} doSearch={search} />
      <List data={list} />
    </div>
  );
}