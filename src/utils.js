export const getImageUrl = (item) => {
  return item.thumbnail && item.thumbnail !== "self"
    ? item.thumbnail
    : "http://via.placeholder.com/100x100";
};
