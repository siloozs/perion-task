import React from "react";

export default React.forwardRef(function SearchFrom({ doSearch }, ref) {
  return (
    <div className="search-form">
      <div className="ui action input">
        <input type="text" placeholder="Search..." ref={ref} />
        <button className="ui button" onClick={doSearch}>
          Search
        </button>
      </div>
    </div>
  );
});