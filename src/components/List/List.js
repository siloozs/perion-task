import React from "react";
import SingleItem from "../SingleItem/SingleItem.js";
export default function List({ data }) {
  return (
    <div className="ui items">
      {data.map((item, idx) => (
        <SingleItem key={idx} item={item.data} />
      ))}
    </div>
  );
}
