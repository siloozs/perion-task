import React from "react";
import { getImageUrl } from "../../utils";
export default function SingleItem({ item }) {
  return (
    <div className="item">
      <div className="image">
        <img src={getImageUrl(item.thumbnail)} alt={item.title} />
      </div>
      <div className="content">
        <a rel="noreferrer" target="_blank" className="header" href={item.url}>
          {item.title}
        </a>
        <div className="description">
          <p>{item.selftext}</p>
        </div>
        <div className="extra">Author: {item.author}</div>
      </div>
    </div>
  );
}
